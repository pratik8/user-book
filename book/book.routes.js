const controller = require('./book.controller');
const router = require('express').Router();

router.post('/save',controller.save);
router.post('/:id/update',controller.updateBook);
router.delete('/:id',controller.deleteBook);
router.get('/:id', controller.getBook);
router.get('', controller.getAllBooks);

module.exports = router;
