const book = require("./book.model");

const service = {
  saveBook(book) {
    return book.save();
  },
  getAllBooks(id) {
    return book.find(
      { isDeleted: false ,userId:id}).exec();
  },
  getBook(id) {
    return book.findById({ _id: id,isDeleted: false }).exec();
  },
  updateBook(id, data) {
    return book.findOneAndUpdate(
      { _id: id },
      { $set: data },
      { new: true }
    ).exec();
  },
  deleteBook(id) {
    return book.findByIdAndUpdate(
      { _id: id },
      { $set: { isDeleted: true } },
      { new: true }
    ).exec();
  },
};
module.exports = service;
