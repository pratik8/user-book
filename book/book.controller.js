const { logger } = require('../utils');
const service = require('./book.services');
const books = require('./book.model');

const controller = {

 async save(req,res){
  try{
    const reqData = new books({
      title:req.body.title,
      isbn:req.body.isbn,
      author:req.body.author,
      userId:req.userId
    })
    console.log(reqData);
    const data= await service.saveBook(reqData);
    const response = {
      statusCode: 200,
      message: 'book save successfully',
      data,
      error: false,
    };
    res.status(200).send(response);
  }catch (error){
    logger.error(error);
    res.status(500).send('Internal server error');
  }
 },
  async getAllBooks(req, res) {
    try {
      const data = await service.getAllBooks(req.userId);
      const response = {
        statusCode: 200,
        message: 'book loaded successfully',
        data,
        error: false,
      };
      res.status(200).send(response);
    } catch (error) {
      logger.error(error);
      res.status(500).send('Internal server error');
    }
  },

  async getBook(req, res) {
    try {
      const book = await service.getBook(req.params.id);
        const response = {
        statusCode: 200,
        message: 'book loaded successfully',
        data,
      };
      res.status(200).send(response);
    } catch (error) {
      res.status(500).send('Internal server error');
    }
  },
  async updateBook(req, res) {
    try {
      const book = {
        title:req.body.title,
        isbn:req.body.isbn,
        author:req.body.author
        };
      const data = await service.updateBook(req.params.id, book);
      const response = {
        statusCode: 200,
        message: 'book updated successfully',
        data,
      };
      res.status(200).send(response);
    } catch (error) {
      res.status(500).send('internal server error');
    }
  },
  async deleteBook(req, res) {
    try {
      const book = await service.deleteBook(req.params.id);
      const response = {
        statusCode: 200,
        message: 'book Deleted',
      };
      res.status(200).send(response);
    } catch (error) {
      logger.log(error);
      res.status(500).send('Internal server error');
    }
  },
  
  async getBook(req, res) {
    try {
      const id = req.params.id;
      const data = await service.getBook(id);
      const response = {
        status: 200,
        message: 'book loaded successfully',
        data,
      };
      res.status(200).send(response);
    } catch (error) {
      logger.log(error);
    }
  },

};

module.exports = controller;
