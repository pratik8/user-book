const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const schemaData = mongoose.Schema({
  title:String,
  isbn:String,
  author:String,
  userId:{ type: Schema.Types.ObjectId, ref: 'Users' },
  createdBy: String,
  updatedBy: String,
  isDeleted: String,
});

module.exports = mongoose.model('Books', schemaData);
