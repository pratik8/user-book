const jwt = require('jsonwebtoken');
const { userService, User } = require('../user');
const service = require('./auth.service');
const config = require('../config');

const controller = {
  async login(req, res) {
    const email = req.body.email;
    const password = req.body.password;
    try {
      const user = await userService.loginByEmail(email);
      if (user && service.validPassword(password, user.password)) {
        const payload = {
          id: user._id,
          email: user.email,
          name: user.name,
        };
        const token = jwt.sign(payload, config.get('jwt.superSecret'), {
          expiresIn: 14404, 
        });
        res.status(200).send({
          success: true,
          message: 'Enjoy your token!',
          token,
        });
      }
    } catch (error) {
      console.log(error);
      res.status(500).send('Internal server error');
    }
  },
 async signUp(req, res) {
    
    try {
      const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: service.generateHash(req.body.password),
      });
      const isUser = await userService.findByEmail(req.body.email);
      if (isUser) {
        const response = {
          status: '202',
          message: 'Email Already Exists',
        };
        res.status(200).send(response);
      } else {
        console.log(user);
        const data = await userService.saveUser(user);
        const response = {
          message: 'user added successfully',
          data,
        };
        res.status(200).send(response);
      }
    } catch (error) {
      res.status(500).send('Internal server error');
    }
  },
};
module.exports = controller;
