const controller = require('./auth.controller');
const router = require('express').Router();

router.post('/signin', controller.login);
router.post('/signup', controller.signUp);

module.exports = router;
