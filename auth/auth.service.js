const bcrypt = require("bcrypt");
const config = require("../config");
const jwt = require("jsonwebtoken");

const service = {
  generateHash: password =>
    bcrypt.hashSync(password, bcrypt.genSaltSync(8), null),
  validPassword: (password, passwordHash) =>
    bcrypt.compareSync(password, passwordHash),

  verifyJWTHeader(req, res, next) {
    const token = req.headers["x-access-token"];
    if (token) {
      // verifies secret and checks exp
      jwt.verify(token, config.get("jwt.superSecret"), (err, decoded) => {
        if (err) {
          return res.json({
            success: false,
            message: "Failed to authenticate token."
          });
        } else {
          req.userId = decoded.id;
          req.email = decoded.email;
          req.userName = decoded.name;
          next();
        }
      });
    } else {
      // if there is no token
      // return an error
      return res.status(403).send({
        success: false,
        message: "No token provided."
      });
    }
  }
};

module.exports = service;
