const { usersRoutes } = require('../user');
const { booksRoutes } = require('../book');
const { authRoutes, authService } = require('../auth');

const initialize = (app) => {
  // This is for CRUD operation on user information
  app.use('/auth', authRoutes);
  app.use(authService.verifyJWTHeader);
  app.use('/users', usersRoutes);
  app.use('/books', booksRoutes);
};


module.exports = {
  initialize,
};
