const mongoose = require('mongoose');
const config = require('../config');

// Use native promises
mongoose.Promise = global.Promise;


const initialize = () => {
  mongoose.connect(config.get('database.host'), { })
    .then(() => {
      console.log(`database connected on ${config.get('database.host')}`);
    })
    .catch((error) => {
      console.log(`Error :${error}`);
      console.log('not able to connect with mongoDB, existing process');
      // not able to connect with mongo DB existing process
      process.exit(0);
    });
};

module.exports = {
  initialize,
};
