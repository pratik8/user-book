const db = require('./db');
const plugins = require('./db.plugins');

// Re Export everything in utils folder
module.exports = {
  db,
  plugins,
};
