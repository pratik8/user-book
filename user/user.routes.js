const controller = require('./user.controller');
const router = require('express').Router();

router.post('/:id/update',controller.updateUser)
router.delete('/:id',controller.deleteUser);
router.get('/name/:name', controller.searchByName);
router.get('/email/:email', controller.searchByEmail);
router.get('/profile', controller.getUserProfile);
router.get('/:id', controller.getUser);
router.get('', controller.getAllUsers);
module.exports = router;
