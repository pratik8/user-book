const mongoose = require('mongoose');


const schema = mongoose.Schema({
  name: String,
  email: String,
  password: String,
  createdBy: String,
  updatedBy: String,
  isDeleted: Boolean,
});

module.exports = mongoose.model('Users', schema);
