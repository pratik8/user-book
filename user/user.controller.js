const { logger } = require('../utils');
// const util = require('util');
// const { authService } = require('../auth');
const User = require('./user.model');
const service = require('./user.services');

// const fileUpload = util.promisify(cloudinary.v2.uploader.upload);

const controller = {

 
  async getAllUsers(req, res) {
    try {
      const data = await service.getAllUsers();
      const response = {
        statusCode: 200,
        message: 'user loaded successfully',
        data,
        error: false,
      };
      res.status(200).send(response);
    } catch (error) {
      logger.error(error);
      res.status(500).send('Internal server error');
    }
  },
  async searchByName(req,res){
    try {
      const name =req.params.name;
      const data = await service.findByName(name);
      const response = {
        statusCode: 200,
        message: 'user search By name',
        data,
      };
      res.status(200).send(response);
    } catch (error) {
      res.status(500).send('Internal server error');
    }
  },
  async searchByEmail(req, res) {
    try {
      const email =req.params.email;
      const data = await service.findByEmail(email);
      const response = {
        statusCode: 200,
        message: 'user email loaded successfully',
        data,
        error: false,
      };
      res.status(200).send(response);
    } catch (error) {
      res.status(500).send('Internal server error');
    }
  },

  async getUser(req, res) {
    try {
      const user = await service.getUser(req.params.id);
        const response = {
        statusCode: 200,
        message: 'user loaded successfully',
        data,
      };
      res.status(200).send(response);
    } catch (error) {
      res.status(500).send('Internal server error');
    }
  },
  async updateUser(req, res) {
    try {
      const user = {
        name: req.body.name,
        email: req.body.email,
        updatedBy: req.userName,
      };
      const data = await service.updateUser(req.params.id, user);
      const response = {
        statusCode: 200,
        message: 'user updated successfully',
        data,
      };
      res.status(200).send(response);
    } catch (error) {
      res.status(500).send('internal server error');
    }
  },
  async getUserProfile(req, res) {
    try {
      const data = await service.getUser(req.userId);
      const response = {
        statusCode: 200,
        message: 'current user Info',
        data,
      };
      res.status(200).send(response);
    } catch (error) {
      logger.error(error);
      res.status(500).send('internal server error');
    }
  },

  async deleteUser(req, res) {
    try {
      const user = await service.deleteUser(req.params.id);
      const response = {
        statusCode: 200,
        message: 'User Deleted',
      };
      res.status(200).send(response);
    } catch (error) {
      res.status(500).send('Internal server error');
    }
  },
  
  async findUserByEmail(req, res) {
    try {
      const email = req.body.email;
      const data = await service.findByEmail(email);
      const response = {
        status: 200,
        message: 'email',
        data,
      };
      res.status(200).send(response);
    } catch (error) {
      logger.log(error);
    }
  },
  async getUser(req, res) {
    try {
      const id = req.params.id;
      const data = await service.getUser(id);
      const response = {
        status: 200,
        message: 'email',
        data,
      };
      res.status(200).send(response);
    } catch (error) {
      logger.log(error);
    }
  },

};

module.exports = controller;
