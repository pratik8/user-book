const User = require("./user.model");
const bcrypt = require("bcrypt");

const service = {
  generateHash(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
  },
  saveUser(user) {
    return user.save();
  },
  findByEmail(emailId) {
    return User.findOne({ email: emailId,isDeleted: false },
      {_id:1, name:1,email:1}
    ).exec();
  },
  loginByEmail(emailId) {
    return User.findOne({ email: emailId,isDeleted: false },
    ).exec();
  },
  getAllUsers() {
    return User.find(
      { isDeleted: false } ,
      {_id:1, name:1,email:1}
    ).exec();
  },
  findByName(name){
    return User.find({name:name,isDeleted: false},
      {_id:1, name:1,email:1}).exec();
  },
  getUser(id) {
    return User.findById({ _id: id,isDeleted: false },
      {_id:1, name:1,email:1}
    ).exec();
  },
  updateUser(id, data) {
    return User.findOneAndUpdate(
      { _id: id },
      { $set: data },
      { new: true }
    ).exec();
  },
  deleteUser(id) {
    return User.findByIdAndUpdate(
      { _id: id },
      { $set: { isDeleted: true } },
      { new: true }
    ).exec();
  },
};
module.exports = service;
