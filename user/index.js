const usersRoutes = require('./user.routes');
const User = require('./user.model');
const userService = require('./user.services');
// Re export
module.exports = { User, usersRoutes, userService };
